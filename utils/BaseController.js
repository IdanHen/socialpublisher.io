var logger = require('winston');


/**
 * create base controller and assign parameters
 * @constructor
 */
function BaseController()
{
    this.params = {
        layout:         this.layout,
        title:          'Social Publisher'
    };
}

BaseController.prototype.getParams = function()
{
    return this.params;
};

BaseController.prototype.setParam = function(_name, _value)
{
    this.params[_name] = _value;
};

module.exports = BaseController;
// module.exports = new BaseController();