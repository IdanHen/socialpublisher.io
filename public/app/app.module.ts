import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule }    from '@angular/http';
import { AppComponent }   from '../components/app.component';
import { FormsModule } from '@angular/forms';
import { SocialNetworksComponent } from '../components/social-networks.component';
import { AccountsListComponent } from '../components/accounts-list.components'
// our services
import { SocialNetworkService }         from '../services/social-network.service';
// routing
// import { AppRoutingModule }     from '../app/app-routing.module';

@NgModule({
    imports:      [
        BrowserModule,
        HttpModule,
        FormsModule
        // AppRoutingModule
    ],
    declarations: [
        AppComponent,
        SocialNetworksComponent,
        AccountsListComponent
    ],
    providers: [ SocialNetworkService ],
    bootstrap:    [ AppComponent ]
})

export class AppModule { }
