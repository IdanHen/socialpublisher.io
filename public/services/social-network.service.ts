import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import { SocialNetwork } from '../models/social-network.model';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class SocialNetworkService
{
    private socialNetworksUrl = 'app/networks';  // URL to web api
    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(private http: Http) { }

    getSocialNetworks(): Promise<SocialNetwork[]>
    {
        return this.http.get(this.socialNetworksUrl)
            .toPromise()
            .then(response => response.json() as SocialNetwork[])
            // .then(response => {
            //     console.log(response.json().data); // I get called: true
            //     return response.json().data;
            // })
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any>
    {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}