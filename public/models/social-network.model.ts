export class SocialNetwork {
    id: number;
    name: string;
    active: boolean;
}