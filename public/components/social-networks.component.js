"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var social_network_service_1 = require('../services/social-network.service');
// import { Router } from '@angular/router';
var SocialNetworksComponent = (function () {
    function SocialNetworksComponent(socialNetworkService) {
        this.socialNetworkService = socialNetworkService;
    }
    SocialNetworksComponent.prototype.ngOnInit = function () {
        this.getSocialNetworks();
    };
    SocialNetworksComponent.prototype.getSocialNetworks = function () {
        var _this = this;
        // this.heroes = this.heroService.getHeroes();
        this.socialNetworkService.getSocialNetworks()
            .then(function (socialNetwork) { return _this.socialNetworks = socialNetwork; });
    };
    ;
    SocialNetworksComponent.prototype.selectNetwork = function ($event, socialNetwork) {
        $event.stopPropagation();
        // reset all networks to false.
        this.socialNetworks.map(function (network) { return network.active = false; });
        socialNetwork.active = true;
    };
    ;
    SocialNetworksComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'social-networks',
            templateUrl: '/html/social-networks.component.html',
            styleUrls: ['../stylesheets/social-networks.component.css']
        }), 
        __metadata('design:paramtypes', [social_network_service_1.SocialNetworkService])
    ], SocialNetworksComponent);
    return SocialNetworksComponent;
}());
exports.SocialNetworksComponent = SocialNetworksComponent;
