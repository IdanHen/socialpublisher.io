import { Component, OnInit  } from '@angular/core';
import { SocialNetwork } from '../models/social-network.model';
import { SocialNetworkService } from '../services/social-network.service';
// import { Router } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'social-networks',
    templateUrl: '/html/social-networks.component.html',
    styleUrls: [ '../stylesheets/social-networks.component.css' ]
})

export class SocialNetworksComponent implements OnInit
{
    socialNetworks: SocialNetwork[];

    constructor(
        private socialNetworkService: SocialNetworkService,
        // private router: Router
    ) { }

    ngOnInit(): void {
        this.getSocialNetworks();
    }

    getSocialNetworks(): void {
        // this.heroes = this.heroService.getHeroes();
        this.socialNetworkService.getSocialNetworks()
            .then(socialNetwork => this.socialNetworks = socialNetwork);
    };

    selectNetwork($event, socialNetwork: SocialNetwork): void {
        $event.stopPropagation();
        // reset all networks to false.
        this.socialNetworks.map(network => network.active = false);
        socialNetwork.active = true;
    };

    // isActive(SocialNetwork): boolean {
    //     console.log('SocialNetwork', SocialNetwork);
    //     return false;
    // }
}
