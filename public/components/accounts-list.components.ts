import { Component, OnInit  } from '@angular/core';
import { Account } from '../models/account.model';
// import { SocialNetworkService } from '../services/social-network.service';
// import { Router } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'accounts-list',
    templateUrl: '/html/accounts-list.component.html',
    styleUrls: [ '../stylesheets/accounts-list.component.css' ]
})

export class AccountsListComponent implements OnInit
{
    accounts: Account[] = [
        {
            id: 1,
            name: "idan",
            isSelected: true
        }, {
            id: 2,
            name: "hili",
            isSelected: true
        },{
            id: 3,
            name: "shiran",
            isSelected: false
        }];

    constructor(
        // private socialNetworkService: SocialNetworkService,
        // private router: Router
    ) { }

    ngOnInit(): void {
        // this.getAccounts();
    }

    getAccounts(): void {

        // this.accounts = [this.account1, this.account2, this.account3];
        // this.heroes = this.heroService.getHeroes();
        // this.socialNetworkService.getSocialNetworks()
        //     .then(socialNetwork => this.socialNetworks = socialNetwork);
    };
}
