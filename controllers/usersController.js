var request = require('request');
var mongoose = require("mongoose");
var logger = require('winston');
var BaseController = require('../utils/BaseController');
var inherits = require('util').inherits;

/**
 * create users controller and assign parameters
 * @constructor
 */
function UsersController()
{
    this.viewsFolder = 'users/';
    this.layout = 'layouts/site';

    this.userModel = require('../models/user'); //mongoose.model('User', userSchema);
    BaseController.call(this);
}
inherits(UsersController, BaseController);

/**
 * GET request - renders the profile page of the user.
 * @param req
 * @param res
 * @param next
 */
UsersController.prototype.profile = function(req, res, next)
{
    res.render(this.viewsFolder + 'profile', this.params);
    // res.render(this.viewsFolder + 'new', { title: 'Add New User' });
};

/**
 * GET request - renders the login page
 * @param req
 * @param res
 * @param next
 */
UsersController.prototype.login = function(req, res, next)
{
    res.render(this.viewsFolder + 'login', this.params);
};

/**
 * removes the local user/password from mongo users collection
 * @param req
 * @param res
 * @param next
 */
UsersController.prototype.removeLocalLogin = function(req, res, next)
{
    var user            = req.user;
    user.local.email    = undefined;
    user.local.password = undefined;
    user.save(function(err) {
        res.redirect('/users/profile');
    });
};

/**
 * GET request - renders the signup page
 * @param req
 * @param res
 * @param next
 */
UsersController.prototype.signup = function(req, res, next)
{
    res.render(this.viewsFolder + 'signup', this.params);
};

/**
 * remove the facebook token from mongo DB users collection
 * @param req
 * @param res
 * @param next
 */
UsersController.prototype.removeFacebookCredentials = function(req, res, next)
{
    logger.debug("removeFacebookCredentials");
    var user            = req.user;
    user.facebook.token = undefined;
    user.save(function(err) {
        res.redirect('/users/profile');
    });
};

UsersController.prototype.removeTwitterCredentials = function(req, res, next)
{
    var user           = req.user;
    user.twitter.token = undefined;
    user.save(function(err) {
        res.redirect('/users/profile');
    });
};

UsersController.prototype.removeGoogleCredentials = function(req, res, next)
{
    var user          = req.user;
    user.google.token = undefined;
    user.save(function(err) {
        res.redirect('/users/profile');
    });
};

/**
 * GET request - logout the user & redirect him to home page.
 * @param req
 * @param res
 * @param next
 */
UsersController.prototype.logout = function(req, res, next)
{
    req.logout();
    res.redirect('/');
};


module.exports = new UsersController();