var request = require('request');
var mongoose = require("mongoose");
var logger = require('winston');
var BaseController = require('../utils/BaseController');
var inherits = require('util').inherits;


/**
 * create users controller and assign parameters
 * @constructor
 */
function AppController()
{
    this.viewsFolder = 'app/';
    this.layout = 'layouts/app';

    this.userModel = require('../models/user'); //mongoose.model('User', userSchema);
    BaseController.call(this);
}
inherits(AppController, BaseController);

/**
 * GET request - renders the profile page of the user.
 * @param req
 * @param res
 * @param next
 */
AppController.prototype.main = function(req, res, next)
{
    this.setParam('user', req.user);
    res.render(this.viewsFolder + 'main', this.params);
    // res.render(this.viewsFolder + 'new', { title: 'Add New User' });
};

/**
 * return a list of networks
 * @param req
 * @param res
 * @param next
 */
AppController.prototype.networks = function(req, res, next)
{
    res.status(200).json([
        {
            id: 1,
            name: 'facebook',
            active: true
        },
        {
            id: 2,
            name: 'twitter',
            active: false
        },
        {
            id: 3,
            name: 'instagram',
            active: false
        }
    ]);
};

module.exports = new AppController();