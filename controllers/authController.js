var request = require('request');
var mongoose = require("mongoose");
var logger = require('winston');
var passport = require('../components/passport');
var BaseController = require('../utils/BaseController');
var inherits = require('util').inherits;

/**
 * create users controller and assign parameters
 * @constructor
 */
function AuthController()
{
    this.userModel = require('../models/user'); //mongoose.model('User', userSchema);
    BaseController.call(this);
}

inherits(AuthController, BaseController);
/**
 * generating a FB connect api call
 */
AuthController.prototype.facebookConnect = function()
{
    logger.debug("sending a facebook connect request");
    passport.getPassport().authenticate('facebook', {
        scope : ['email']
        // display: 'popup'
    });
};

/**
 * after FB connects - we get a api call to here
 */
AuthController.prototype.facebookCallback = function()
{
    logger.debug("received facebook callback");
    passport.getPassport().authenticate('facebook', {
        successRedirect : '/users/profile',
        failureRedirect : '/'
    });
};


module.exports = new AuthController();