var request = require('request');
var mongoose = require("mongoose");
var logger = require('winston');
var BaseController = require('../utils/BaseController');
var inherits = require('util').inherits;

/**
 * create users controller and assign parameters
 * @constructor
 */
function SiteController()
{
    this.viewsFolder = 'site/';
    this.layout = 'layouts/site';

    this.userModel = require('../models/user'); //mongoose.model('User', userSchema);
    BaseController.call(this);
}
inherits(SiteController, BaseController);

/**
 * route: users/ & users/list
 * gets the list of users and display them
 * @param req
 * @param res
 * @param next
 */
SiteController.prototype.home = function(req, res, next)
{
    res.render(this.viewsFolder + 'home', this.params);
};

SiteController.prototype.about = function(req, res, next)
{
    res.render(this.viewsFolder + 'about', this.params);
};

module.exports = new SiteController();