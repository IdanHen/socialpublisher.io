var request = require('request');
var mongoose = require("mongoose");
var logger = require('winston');

function Authenticator()
{
}

Authenticator.prototype.allowAll = function(req, res, next)
{
    return next();
};

Authenticator.prototype.isLoggedIn = function(req, res, next)
{
    // if user is authenticated in the session, carry on
    // logger.debug('isLoggedIn');
    // logger.debug('req.session', req.session);
    if (req.isAuthenticated())
    {
        logger.debug('user Authenticated!');
        return next();
    }

    // if they aren't redirect them to the home page
    res.redirect('/users/login');
};

Authenticator.prototype.isGuest = function(req, res, next)
{
    // if user is authenticated in the session, carry on
    // logger.debug('isGuest hit');
    // logger.debug('req.session', req.session);
    if (req.isAuthenticated())
    {
        logger.debug('known user, lets redirect');
        res.redirect('/users/profile');
        return;
    }

    return next();
    // if they aren't redirect them to the home page
};


module.exports = new Authenticator();