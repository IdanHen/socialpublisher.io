// =============  set up  ======================================================
var express = require('express');
var redis   = require("redis");
var app = express(); // application express server
var config = require('config'); // application config
var mongo = require('mongodb'); // mongoDB
var mongoose = require("mongoose"); // mongo DB handler
var db = mongoose.connection;
var passport = require('passport'); // authentication passport package
var flash    = require('connect-flash'); // session flash
var expressWinston = require('express-winston'); // express logger connector
var logger = require('winston'); // application logger
var path = require('path');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session      = require('express-session');
var redisStore = require('connect-redis')(session); // creating redis store for the session
var passportComponent = require('./components/passport'); // pass passport for configuration
var methodOverride = require('method-override'); // support DELETE & PUT
var hbs = require('hbs');
var fs = require('fs');

// configure logger to output only to file !
logger.level = config.get('logger.level'); // todo: change to get from config
logger.add(logger.transports.File, {
    dirname: 'logs',
    filename: 'social.log',
    json: false,
    showLevel: true, //{ error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5 }
    prettyPrint: true,
    colorize: false,
    maxFiles: 12
    // maxsize: '1m' // Supports 1024, 1k, 1m, 1g. Defaults to 100m
}).remove(logger.transports.Console);

// ================ redis setup =============================================
var redisClient  = redis.createClient(config.get('redis.port'), config.get('redis.host'));
redisClient.auth(config.get('redis.pass'));

redisClient.on('error', function(err) {
    logger.error('redis client failed to connect!!! ', err);
});


// ================ mongoDB setup =============================================
db.on("error", logger.error.bind(logger,"mongoose connection error"));

var mongoDBoptions = {
    // db: { native_parser: true },
    // server: { poolSize: 5 },
    // replset: { rs_name: 'myReplicaSetName' },
    user: config.get('db.mongo.user'),
    pass: config.get('db.mongo.pass')
};
mongoose.connect(config.get('db.mongo.host') + config.get('db.mongo.dbName') , mongoDBoptions);

// ==================    view engine setup =====================================
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
var partialsDir = path.join(__dirname, 'views/partials');//__dirname + '/../views/partials';
var file_names = fs.readdirSync(partialsDir);
file_names.forEach(function (filename) {
    var matches = /^([^.]+).hbs$/.exec(filename);
    if (!matches) {
        return;
    }
    var name = matches[1];
    var template = fs.readFileSync(partialsDir + '/' + filename, 'utf8');
    hbs.registerPartial(name, template);
});

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
// ================== set up our express application ============================
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// support for DELETE & PUT
app.use(methodOverride(function(req, res){
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method;
        delete req.body._method;
        return method;
    }
}));

app.use(cookieParser());
app.use(require('node-sass-middleware')({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: true,
  sourceMap: true
}));
app.use(express.static(path.join(__dirname, 'public'))); // static files go to public folder
app.use('/bower_components',  express.static(__dirname + '/bower_components')); // bower static files ( js , css , etc . )
app.use('/node_modules',  express.static(__dirname + '/node_modules')); // angular files from here as static

// ==================    passport   ============================================
passportComponent.setPassport(passport); // pass passport for configuration
app.use(session({
        store: new redisStore({
            host: config.get('redis.host'),
            port: config.get('redis.port'),
            client: redisClient
            // ttl :  260
        }),
        secret: config.get("session.secret"),
        // create new redis store.
        // cookie: {httpOnly: false, secure: false, maxAge: 2419200000},
        cookie: { httpOnly: true, secure: false, maxAge: 2419200000},
        resave: false,
        saveUninitialized: false
    })
); // session secret

app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(function(req, res, next) {
    // res.locals.user = req.user; // This is the important line
    res.locals.req = req;
    res.locals.authenticated = req.isAuthenticated();

    next();
});
// app.use(flash()); // use connect-flash for flash messages stored in session

// ==================  application access & error files to winston log ============
app.use(expressWinston.logger({
    transports: [
        new logger.transports.File({
            dirname: 'logs',
            filename: 'expressAccess.log',
            json: false,
            showLevel: true,
            prettyPrint: false,
            colorize: false,
            maxFiles: 12
            // maxsize: 1024 // Supports 1024, 1k, 1m, 1g. Defaults to 100m
        })
    ]
}));

// catch 404 and forward to error handler
// Place the express-winston errorLogger after the router.
app.use(expressWinston.errorLogger({
    transports: [
        new logger.transports.File({
            dirname: 'logs',
            filename: 'expressErrors.log',
            showLevel: true,
            prettyPrint: false,
            colorize: false,
            maxFiles: 12,
            maxsize: '1m' // Supports 1024, 1k, 1m, 1g. Defaults to 100m
        })
    ]
}));

// ==================    routes   ==============================================
var routes = require('./routes/indexRouter');
var users = require('./routes/usersRouter');
var auth = require('./routes/authRouter'); // the signup process
var connect = require('./routes/connectRouter'); // the signup process
var socialApp = require('./routes/socialAppRouter'); // the signup process
app.use('/', routes);
app.use('/site', routes);
app.use('/users', users);
app.use('/auth', auth);
app.use('/connect', connect);
app.use('/app', socialApp);

module.exports = app;
