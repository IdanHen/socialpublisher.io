/**
 * Created by idan on 18/10/2016.
 */
var mongoose = require("mongoose");
var bcrypt   = require('bcrypt-nodejs');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

function UserModel()
{
    this.init();
}

/**
 * init the model by creating the schema, each model we create will be able to set these schema parameters
 */
UserModel.prototype.init = function()
{
    this.userSchema = new Schema({


        local            : {
            email        : String,
            password     : String,
            displayName  : String
        },
        facebook         : {
            id           : String,
            token        : String,
            email        : String,
            name         : String
        },
        twitter          : {
            id           : String,
            token        : String,
            displayName  : String,
            username     : String
        },
        google           : {
            id           : String,
            token        : String,
            email        : String,
            name         : String
        }

        // username: { type: String }
        // , email: String
        //,album: { type: String, required: true, trim: true }
        // , releaseYear: Number
        // , hasCreditCookie: Boolean
    });

    this.userSchema.methods.generateHash = function(password) {
        return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
    };

    this.userSchema.methods.validPassword = function(password) {
        return bcrypt.compareSync(password, this.local.password);
    };
};

/**
 * creating the model to be used by the controller
 * @returns {*|Model|Aggregate}
 */
UserModel.prototype.createModel = function()
{
    return mongoose.model('User', this.userSchema)
};

module.exports = (new UserModel()).createModel();