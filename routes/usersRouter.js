var express = require('express');
var router = express.Router();
var usersController = require('../controllers/usersController');
var authenticator = require('../components/authenticator');
var passportComponent = require('../components/passport'); // pass passport for configuration

// open routes
// router.get('/', usersController.get.bind(usersController));
router.get('/login', usersController.login.bind(usersController));
router.get('/signup', usersController.signup.bind(usersController));

// requires login
router.get('/logout', authenticator.isLoggedIn.bind(authenticator), usersController.logout.bind(usersController));
router.get('/profile', authenticator.isLoggedIn.bind(authenticator), usersController.profile.bind(usersController));

router.post('/signup', passportComponent.getPassport().authenticate('local-signup', {
    successRedirect : '/users/profile', // redirect to the secure profile section
    failureRedirect : '/users/signup', // redirect back to the signup page if there is an error
    failureFlash : true // allow flash messages
}));
router.post('/login', passportComponent.getPassport().authenticate('local-login', {
    successRedirect : '/users/profile', // redirect to the secure profile section
    failureRedirect : '/users/login', // redirect back to the signup page if there is an error
    failureFlash : true // allow flash messages
}));

module.exports = router;
