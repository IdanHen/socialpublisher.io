var express = require('express');
var router = express.Router();
var passportComponent = require('../components/passport'); // pass passport for configuration

// =====================================
// FACEBOOK ROUTES ======================
// =====================================
// send to facebook to do the authentication
router.get('/facebook', passportComponent.getPassport().authenticate('facebook', {
    scope: ['email'],
    display: 'popup'
}));
// handle the callback after facebook has authenticated the user
router.get('/facebook/callback', passportComponent.getPassport().authenticate('facebook', {
    successRedirect : '/users/profile',
    failureRedirect : '/'
}));


// =====================================
// TWITTER ROUTES ======================
// =====================================
// route for twitter authentication and login
router.get('/twitter', passportComponent.getPassport().authenticate('twitter'));

// handle the callback after twitter has authenticated the user
router.get('/twitter/callback',passportComponent.getPassport().authenticate('twitter', {
        successRedirect : '/users/profile',
        failureRedirect : '/'
    }));

// =====================================
// GOOGLE ROUTES =======================
// =====================================
// send to google to do the authentication
// profile gets us their basic information including their name
// email gets their emails
router.get('/google', passportComponent.getPassport().authenticate('google', { scope : ['profile', 'email'] }));

// the callback after google has authenticated the user
router.get('/google/callback',passportComponent.getPassport().authenticate('google', {
        successRedirect : '/users/profile',
        failureRedirect : '/'
    }));


module.exports = router;
