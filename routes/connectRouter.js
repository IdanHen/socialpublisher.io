var express = require('express');
var router = express.Router();
var logger = require('winston');
var passportComponent = require('../components/passport'); // pass passport for configuration
var authenticator = require('../components/authenticator');
var usersController = require('../controllers/usersController');

// =============================================================================
// AUTHORIZE (ALREADY LOGGED IN / CONNECTING OTHER SOCIAL ACCOUNT) =============
// =============================================================================

// =====================================
// LOCAL ROUTES ==========+=============
// =====================================
router.get('/local', usersController.login.bind(usersController));
router.post('/local', passportComponent.getPassport().authenticate('local-signup', {
    successRedirect : '/users/profile', // redirect to the secure profile section
    failureRedirect : '/connect/local', // redirect back to the signup page if there is an error
    failureFlash : true // allow flash messages
}));
router.delete('/local', authenticator.isLoggedIn.bind(authenticator), usersController.removeLocalLogin.bind(usersController));

// =====================================
// FACEBOOK ROUTES =====================
// =====================================
// send to facebook to do the authentication
router.get('/facebook', passportComponent.getPassport().authorize('facebook', { scope : 'email' }));

// handle the callback after facebook has authorized the user
router.get('/facebook/callback', passportComponent.getPassport().authorize('facebook', {
        successRedirect : '/users/profile',
        failureRedirect : '/'
}));
// handle removal of FB token for user
router.delete('/facebook', authenticator.isLoggedIn.bind(authenticator), usersController.removeFacebookCredentials.bind(usersController));

// =====================================
// TWITTER ROUTES ======================
// =====================================
// send to twitter to do the authentication
router.get('/twitter', passportComponent.getPassport().authorize('twitter', { scope : 'email' }));

// handle the callback after twitter has authorized the user
router.get('/twitter/callback', passportComponent.getPassport().authorize('twitter', {
        successRedirect : '/users/profile',
        failureRedirect : '/'
}));
// handle removal of twitter token for user
router.delete('/twitter', authenticator.isLoggedIn.bind(authenticator), usersController.removeTwitterCredentials.bind(usersController));

// =====================================
// GOOGLE ROUTES ======================
// =====================================
// send to google to do the authentication
router.get('/google', passportComponent.getPassport().authorize('google', { scope : ['profile', 'email'] }));

// the callback after google has authorized the user
router.get('/google/callback', passportComponent.getPassport().authorize('google', {
        successRedirect : '/users/profile',
        failureRedirect : '/'
}));
// handle removal of google token for user
router.delete('/google', authenticator.isLoggedIn.bind(authenticator), usersController.removeGoogleCredentials.bind(usersController));

module.exports = router;
