var express = require('express');
var router = express.Router();
var siteController = require('../controllers/siteController');
var authenticator = require('../components/authenticator');

// home page for guests
router.get('/', authenticator.allowAll.bind(authenticator), siteController.home.bind(siteController));
// access by all users
router.get('/about', siteController.about.bind(siteController));

module.exports = router;
