var express = require('express');
var router = express.Router();
var appController = require('../controllers/appController');
var authenticator = require('../components/authenticator');

// home page for guests
router.get('/main', authenticator.isLoggedIn.bind(authenticator), appController.main.bind(appController));
router.get('/networks', authenticator.isLoggedIn.bind(authenticator), appController.networks.bind(appController));



module.exports = router;
